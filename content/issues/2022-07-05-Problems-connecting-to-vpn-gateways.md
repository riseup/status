---
title: Problems connecting to vpn gateways
date: 2022-07-05 11:53:45
resolved: true
resolvedWhen: 2022-08-16 10:18:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - RiseupVPN
section: issue
---

- **Update**: A [new Windows version is
available](https://downloads.leap.se/RiseupVPN/windows/RiseupVPN-installer-0.21.11-47-g7c5a8fa.exe)
which should resolve any issues connecting for Windows users. If you have
issues, please [report
them](https://0xacab.org/leap/bitmask-vpn/-/issues/678#note_432238) - *August
16, 10:18 UTC*

- **Update**: Windows work is ongoing, our code-signing certificate has been
re-issued, this took longer than expected due to the verification
requirements. The person who can update the Windows client will start working on
this in a few days. Apologies that this has taken longer than expected - *July
26, 13:57 UTC*

- **Identified**: The version of RiseupVPN for Windows was built with an older and
unsupported version of the underlying cryptographic library. A new version of
the Windows client is being prepared to fix this issue. You will need to update
when it becomes available. We are thinking of you, and we are sorry for the
trouble, but please be patient as we wrestle with the Windows beast. Lots of
love! - *July 7, 11:53 UTC*

- **Identified**: If you are an Android user, please go to your App Storage, find
the RiseupVPN app, stop it, and then click both the "Clear Storage" and "Clear
Cache". Then attempt to connect again. - *July 7, 11:53 UTC*

- **Identified**: Mac: If you uninstall RiseupVPN, and then go download the latest
version and install that, it should resolve your issue! - *July 7, 11:53 UTC*
