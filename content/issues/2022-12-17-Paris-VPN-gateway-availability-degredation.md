---
title: Paris VPN gateway availability degredation
date: 2022-12-17 23:47:51
resolved: true
resolvedWhen: 2023-01-03 15:25:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - RiseupVPN
section: issue
---

- 15:25 Service was restored after the machine was rebuilt.

One of our Paris gateways has suffered a fatal hardware problem and will not come back. We are working to replace it, but until we are able, we
will have one less gateway available in that region. This will result in slightly degraded VPN performance as more resources must be shared.
