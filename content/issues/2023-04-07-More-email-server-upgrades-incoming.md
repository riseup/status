---
title: More email servers upgrades incoming
date: 2023-04-08 17:00:00
resolved: true
resolvedWhen: 2023-04-08 18:30:00
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
section: issue
---

- All work was done, systems should be performing normally!

- We are performing a second round of upgrades, there might be some minutes where the e-mail service is inaccessible.

These will be done on April 8th, 15:00 UTC. Please be patient!
