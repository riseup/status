---
title: Pad disruption
date: 2023-01-05 20:16:21
resolved: true
resolvedWhen: 2023-01-15 15:42:42
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - Etherpad
section: issue
---

Etherpad service is experiencing some disruptions today, we are working on stabilizing things.
