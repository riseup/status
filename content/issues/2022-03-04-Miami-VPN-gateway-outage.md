---
title: Miami VPN gateway outage
date: 2022-03-05 01:31:28
resolved: true
resolvedWhen: 2022-03-07 08:00:00
severity: down
affected:
  - RiseupVPN
section: issue
---

It appears that the network routes have been restored and the gateway should be functional now.

Our Miami gateway is offline, we are investigating the cause. As we know more, we will update you.
