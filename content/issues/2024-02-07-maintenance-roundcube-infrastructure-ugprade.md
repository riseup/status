---
title: "maintenance: roundcube infrastructure ugprade"
date: 2024-02-07 15:17:22
resolved: true
resolvedWhen: 2024-02-07 16:40:00
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
section: issue
---

Everything want well, you might have noticed a small roundcube downtime, but
otherwise there should not be any issue. If you have some issue with roundcube,
please visit [https://support.riseup.net](https://support.riseup.net), and open
an issue.

---

We will perform some upgrades on the Roundcube infrastructure. There should not
be long lasting outage, but there might be some small glitches while we reboot
the web servers.
