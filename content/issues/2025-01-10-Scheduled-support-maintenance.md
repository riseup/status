---
title: Scheduled support maintenance
date: 2025-01-13 14:00:00
resolved: true
resolvedWhen: 2025-01-13 18:05:00
severity: notice
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
section: issue
---

On Monday January 13 2025 at 12.00 UTC we will start the replacement of the
helpdesk software. We will need some time to set everything up and test that
it's functioning as expected - if it all goes well it shouldn't take long. Once
the new helpdesk is available, it will be possible to receive support from us
again!
