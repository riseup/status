---
title: pad service down
date: 2022-12-20 17:13:44
resolved: true
resolvedWhen: 2022-12-19 20:11:24
severity: down
# REMOVE THE LINES NOT AFFECTED
affected:
  - Etherpad
section: issue
---

The etherpad service has been restored.

Etherpad service is currently down, we are investigating and should have it resolved shortly.

