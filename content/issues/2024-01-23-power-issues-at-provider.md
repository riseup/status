---
title: power issues at provider
date: 2024-01-23 19:35:10
resolved: true
resolvedWhen:  2024-01-24 00:00:00
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
  - Mailing Lists
section: issue
---

All issues should be resolved

Email is backlogged due to the outage, it is being processed in batches and will be delivered. 

Power has been restored, an inventory of circuit draws and a rebalancing of circuit loads happened. Things are back to normal now, sorry for the interruption!

Someone is on-site resolving issue now. Services are starting to return to normal, but not everything is resolved yet.

Due to an overloaded circuit at our datacenter, several riseup services are currently unavailable. We will update this status information when we have an ETA.
