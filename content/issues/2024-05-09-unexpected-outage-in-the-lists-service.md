---
title: unexpected outage in the lists service
date: 2024-05-09 19:10:17
resolved: true
resolvedWhen: 2024-05-09 23:00:00
severity: down
# REMOVE THE LINES NOT AFFECTED
affected:
  - Mailing Lists
section: issue
---

Our list server is having a hardware problem, we are aware of the situation and working to restore it. But it's unclear when it will be generailly
