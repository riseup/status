---
title: Possible network issues due to DDoS
date: 2021-10-26 19:09:54
resolved: true
resolvedWhen: 2021-10-19 12:10:38
severity: resolved
severity: notice
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

We and other e-mail providers have been subject to a DDoS attack disrupting our services. We received an e-mail blackmailing us: if we don't pay, they will continue to attack us. We will not pay, we can't accept blackmail and we will do everything possible in our hands to mitigate the effects.

A distributed denial-of-service (DDoS) is an attack aiming to make one service unavailable for others, using multiple sources for the attack makes it very complex to handle. We have taken several meassures against them, and we keep improving our systems to resist them but it's still possible to face one that can produce a disruption in our services.

In the case this happens we want to let you know the following:

- There might be problems connecting to our services. Please wait a little and try again later.
- There is no risk for your data.
- We will keep this website ([riseupstatus.net](https://riseupstatus.net)) updated
- We will be also tweeting updates [@riseupnet](https://twitter.com/riseupnet)
- We will be actively working to minimize as much as possible this attack and get things back to normal as soon as possible.
