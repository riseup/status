---
title: partial outage in 0xacab pages
date: 2024-10-17 18:28:10
resolved: true
resolvedWhen: 2024-10-30 00:00:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - 0xacab.org
section: issue
---

The issue at hand was resolved

We have a problem with the domain itcouldbewor.se, it's unclear when it's going to be resolved. If you rely on it for your project, we are sorry for the problems this is creating.
