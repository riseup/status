---
title: etherpad service under heavy load
date: 2022-02-25 19:34:09
resolved: true
resolvedWhen: 2022-03-01 08:00:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - Etherpad
section: issue
---

Our etherpad service is experiencing unusual heavy usage and may result in some
instability for users. We are aware of the issue and working on mitigation, and
will provide further updates as we are able.
