---
title: Scheduled network outage today
date: 2021-10-14 13:42:44
resolved: true
resolvedWhen: 2021-10-14 18:12:54
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

Due to equipment incompatibility, this work was suspended and will be rescheduled at a later time.

Today at 10am Pacific, 1pm Eastern, 17h UTC, there will be a temporary interruption in the network. It will affect all services, and is expected to only take a few minutes. Please plan accordingly.

During this time, you will not be able to reach many of our services, but there will be no loss of emails.
