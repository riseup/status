---
title: Windows RiseupVPN users need to upgrade to connect
date: 2021-12-28 16:45:45
resolved: true
resolvedWhen: 2022-02-02 14:37:57
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - RiseupVPN
section: issue
---

Having trouble connecting to RiseupVPN and running Windows - time to upgrade!

If you are using Windows and are unable to connect to RiseupVPN, then you need to [upgrade your client](https://downloads.leap.se/RiseupVPN/windows/RiseupVPN-win-latest.exe). 

The reason for this requirement is because a low-risk vulnerability was discovered in the Qt Installer Framework, which is used by RiseupVPN which can be triggered on RiseupVPN release previous to 0.21.11. We consider the vulnerability to be low risk, because several conditions are needed to trigger the vulnerability (original installation in a non-standard path, plus a non-privileged malicious user having access to the machine to overwrite the openvpn executable).

Thanks to researchers at [Tenable](https://tenable.com) for originally reporting [this bug](https://0xacab.org/leap/bitmask-vpn/-/issues/569) this bug.
