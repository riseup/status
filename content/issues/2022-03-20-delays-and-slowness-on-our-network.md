---
title: delays and slowness on our network
date: 2022-03-20 22:52:58
resolved: true
resolvedWhen: 2022-03-25 13:52:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

The issues affecting our network have been resolved!

You may experience some slow loading on some of our services, we are investigating and will update this as we know more.
