---
title: Connectivity
date: 2021-08-23 15:43:59
resolved: true
resolvedWhen: 2021-08-23 18:22:00
severity: down
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

- We believe the issue is fixed, but we are going to keep monitoring it closely. Thanks for your patience!

- We found out problems with one of the internet providers we use generating the outage. We are still working on this issue but services should be going back to normal. We are and will be monitoring closely this issue.

- We are investigating a network outage tha affects almost all of our services. We are already working on get things back to normal, please hold on a little.
