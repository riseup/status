---
title: Ongoing mailing list scheduled maintenance
date: 2021-09-26 17:10:27
resolved: true
resolvedWhen: 2021-09-26 19:13:27
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - Mailing Lists
section: issue
---

The list service has been restored to normal operation, and the maintenance window has closed. If you experience problems, please [file a help ticket](https://support.riseup.net)


Today we are working on some necessary upgrades on the list service. This will result in disruptions to our list service.

Disruptions expected: 

- list mail delivery: any message you send will be queued for later delivery. Do not resend your message!
- https://lists.riseup.net will show a maintenance page until completed
