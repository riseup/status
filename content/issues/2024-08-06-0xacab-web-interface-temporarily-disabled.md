---
title: 0xacab web interface temporarily disabled
date: 2024-08-06 14:44:12
resolved: true
resolvedWhen: 2024-08-06 16:41:12
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - 0xacab.org
section: issue
---

Apologies for the interruption in service. There appear to still be people on the internet who don't know how to play well with others.

The web interface for 0xacab is temporarily disabled, no estimated time to get it back. SSH access and E-mail actions are expected to keep working.
