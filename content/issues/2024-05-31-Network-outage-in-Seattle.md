---
title: Network outage in Seattle
date: 2024-05-31 16:48:04
resolved: true
resolvedWhen: 2024-05-31 17:35:00
severity: down
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

We are experiencing a network outage in Seattle, most riseup services are inaccessible. The upstream bandwidth provider's router went down and they are working on it. No ETA for repair yet.
