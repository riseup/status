---
title: Certificates changed, some configuration changes may be necessary
date: 2021-09-17 16:01:42
resolved: true
resolvedWhen: 2021-09-26 17:15:43
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
section: issue
---

- As of today, [Riseup has changed our primary certificate](https://riseup.net/en/security/network-security/certificates) that we have been using since 2019. 

- If you were pinning a certificate fingerprint, this will no longer work.

- Sometimes thunderbird gets confused and needs to be restarted!

- Connecting via the onion address? Consider changing your configuration to use the non-TLS ports (port 143 for IMAP, port 110 for POP3, port 25 for SMTP) without connection security. When using onion addresses, your connection is already secure.

