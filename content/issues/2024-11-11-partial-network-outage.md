---
title: partial network outage
date: 2024-11-12 03:36:50
resolved: true
resolvedWhen: 2024-11-12 21:23:42
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - RiseupVPN
  - 0xacab.org
section: issue
---

Routing issues resolved.

We are experiencing a network routing issue with one of our networks on one of our network providers. This mostly affects non-core riseup services and 3rd party hosted servers. A support issue has been submitted to the provider, they are usually pretty fast at responding/fixing, but no ETA for a fix currently.
