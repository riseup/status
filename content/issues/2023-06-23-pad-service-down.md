---
title: pad service down
date: 2023-06-23 18:49:32
resolved: true
resolvedWhen: 2023-06-25 15:00:00
severity: down
# REMOVE THE LINES NOT AFFECTED
affected:
  - Etherpad
section: issue
---

- Pad was restored, after over a day of downtime. We are sorry for the troubles.

Our pad service is for unexpected problems. The issue has been identified and we are in the process of fixing it, but it might take several hours before it can be restablished.

Until then, please try use another pad service such as the [one provided by sistemli](https://pad.systemli.org/).
