---
title: webmail maintenance in progress
date: 2022-04-10 14:43:46
resolved: true
resolvedWhen: 2022-04-11 00:41:16
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
section: issue
---

This maintenance was completed. If you still experience issues, please send us a help request!

If you are experiencing issues connecting to our webmail (https://mail.riseup.net), this is because we are currently performing some maintenance on the service. It will be back soon!
