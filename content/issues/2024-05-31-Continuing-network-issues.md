---
title: Continuing
date: 2024-05-31 20:26:26
resolved: true
resolvedWhen: 2024-06-01 12:00:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

The general network issue was solved, but there are still problems with our network provider's routing so riseup services are inaccessible from some places on the internet. The provider is investigating.
