---
title: e-mail delivery interrupted
date: 2021-09-30 14:43:49
resolved: true
resolvedWhen: 2021-09-30 15:12:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
section: issue
---

- E-mail should be delivered as usual, we had a little problem with one of our encryption certificates.

- We are currently investigating an issue with e-mail delivery. E-mail can take a little longer to be delivered.
