---
title: Scheduled Pad Maintenance
date: 2022-10-14 14:20:00
resolved: true
resolvedWhen: 2022-10-15 16:30:00
severity: notice
# REMOVE THE LINES NOT AFFECTED
affected:
  - Etherpad
section: issue
---

- 16.30 There weren't any issues and the maintenance was done quicker than we expected. Happy writing!

We need to perform more work in our [pad service](https://pad.riseup.net), from Oct 14 14:00 (_UTC_) to 16:00 you will not be able to use or access any document on it while do some changes to make it more robust. We are sorry for any trouble this can cause.