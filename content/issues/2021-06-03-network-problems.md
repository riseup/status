---
title: network problems
date: 2021-06-03 19:39:11
resolved: true
resolvedWhen: 2021-06-03 19:59:03
severity: resolved
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

We tracked down the networking issues and were able to resolve them.

Network disruption impacting most services detected. We are aware of the problem
and will update this event as we know more.
