---
title: Scheduled network outage today
date: 2021-10-22 11:54:02
resolved: true
resolvedWhen: 2021-10-22 12:10:38
severity: resolved
# REMOVE THE LINES NOT AFFECTED
affected:
  - General
  - E-Mail
  - Mailing Lists
  - Etherpad
  - RiseupVPN
  - 0xacab.org
section: issue
---

Everything went well, thanks for your patience!

Today at 10am Pacific, 1pm Eastern, 17h UTC, there will be a temporary interruption in the network. It will affect all services, and is expected to only take approximately 10 minutes. Please plan accordingly.

During this time, you will not be able to reach many of our services, but there will be no loss of emails.
