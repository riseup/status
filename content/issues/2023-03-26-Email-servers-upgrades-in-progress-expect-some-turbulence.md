---
title: Email servers upgrades in progress, expect some turbulence.
date: 2023-03-26 13:24:27
resolved: true
resolvedWhen: 2023-03-26 19:44:00
severity: disrupted
# REMOVE THE LINES NOT AFFECTED
affected:
  - E-Mail
section: issue
---

We are working on upgrading some of our core email infrastructure. While this happens, some users will experience some disruptions when
attempting to connect. These will resolved as soon as possible, but have no fear, your email is being queued for delivery, so you will
not lose any incoming email.

19:44@utc: systems should be working again, we are monitoring the situation to see if there are any issues.
